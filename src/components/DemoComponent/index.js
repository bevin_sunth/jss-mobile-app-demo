import React from 'react';
import PropTypes from 'prop-types';
import { View, Text as NativeText } from 'react-native';
import { Image, Text, RichText } from '@sitecore-jss/sitecore-jss-react-native';
import styles, { richTextStyles } from './styles';

const DemoComponent = ({ fields }) => (
  <View style={styles.container}>
    <View style={styles.body}>
      <Image media={fields.bannerImage} height="551" width="304" />
    </View>
  </View>
);

DemoComponent.propTypes = {
  bannerImage: PropTypes.shape({
    value: PropTypes.shape({
      // This will be a number in disconnected mode (see dataService.disconnected.js), string in connected
      src: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      alt: PropTypes.string,
    }),
  }),
};

// DemoComponent.defaultProps = {
//   copyright: 'Copyright Sitecore A/S test',
// };

export default DemoComponent;
