import { addComponent, CommonFieldTypes } from '@sitecore-jss/sitecore-jss-manifest';

export default (manifest) => {
  addComponent(manifest, {
    name: 'DemoComponent',
    displayName: 'DemoComponent',
    placeholders: ['jss-main'],
    fields: [
      { name: 'heading', type: CommonFieldTypes.SingleLineText },
      { name: 'bannerImage', type: CommonFieldTypes.Image },
    ],
  });
};
